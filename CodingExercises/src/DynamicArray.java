import java.util.Arrays;

//Exercice link: https://neetcode.io/problems/dynamicArray

class DynamicArray {

    private int[] dynamicArray;
    private int length;
    private int capacity;

    public DynamicArray(int capacity) {

        if (capacity > 0) {
            dynamicArray = new int[capacity];
            this.length = 0;
            this.capacity = capacity;
        } else {
            System.out.println("Capacity must be bigger than 0!");
        }

    }

    public int get(int i) {

        return dynamicArray[i];
    }

    public void set(int i, int n) {
        dynamicArray[i] = n;
    }

    public void pushback(int n) {

        if (length == capacity) {
            resize();
        }

        dynamicArray[length] = n;
        this.length++;
    }

    public int popback() {

        int holderVariable = dynamicArray[length - 1];
        length--;
        dynamicArray[length] = -1;
        return holderVariable;
    }

    private void resize() {
        capacity = capacity * 2;
        int[] holderArray = new int[length];

        for (int i = 0; i < length; i++) {
            holderArray[i] = dynamicArray[i];
        }

        dynamicArray = new int[capacity];

        for (int i = 0; i < holderArray.length; i++) {
            dynamicArray[i] = holderArray[i];
        }
    }

    public int getSize() {

        return length;
    }

    public int getCapacity() {

        return capacity;
    }

    @Override
    public String toString() {
        return "DynamicArray{" +
                "dynamicArray=" + Arrays.toString(dynamicArray) +
                '}';
    }
}

class Main {
    public static void main(String[] args) {

        // Test Case 1
        // Input: ["Array", 1, "getSize", "getCapacity"]
        // Expected Output: [null,0,1]
        System.out.println("--- Test Case 1 ---");
        DynamicArray dynamicArray = new DynamicArray(1);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());

        // Test Case 2
        // Input: ["Array", 5, "getSize", "getCapacity"]
        // Expected Output: [null,0,5]
        System.out.println("\n--- Test Case 2 ---");
        dynamicArray = new DynamicArray(5);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());

        // Test Case 3
        // Input: ["Array", 3, "pushback", 1, "pushback", 2, "pushback", 3, "get", 0, "get", 1, "get", 2]
        // Expected Output: [null,null,null,null,1,2,3]
        System.out.println("\n--- Test Case 3 ---");
        dynamicArray = new DynamicArray(3);
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        dynamicArray.pushback(3);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.get(0));
        System.out.println(dynamicArray.get(1));
        System.out.println(dynamicArray.get(2));

        // Test Case 4
        System.out.println("\n--- Test Case 4 ---");
        dynamicArray = new DynamicArray(4);
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        dynamicArray.set(0, 0);
        System.out.println(dynamicArray);
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.get(0));
        System.out.println(dynamicArray.get(1));
        System.out.println(dynamicArray.getCapacity());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray);

        // Test Case 5
        // Input: ["Array", 5, "pushback", 1, "pushback", 2, "pushback", 3, "popback", "popback", "get", 0]
        // Expected Output: [null,null,null,null,3,2,1]
        System.out.println("\n--- Test Case 5 ---");
        dynamicArray = new DynamicArray(5);
        System.out.println(dynamicArray);
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        dynamicArray.pushback(3);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.get(0));

        // Test Case 6
        // Input: ["Array", 2, "pushback", 0, "pushback", 1, "pushback", 2, "getSize", "getCapacity"]
        // Expected Output: [null,null,null,null,3,4]
        System.out.println("\n--- Test Case 6 ---");
        dynamicArray = new DynamicArray(2);
        dynamicArray.pushback(0);
        System.out.println(dynamicArray);
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());

        // Test Case 7
        // Input: ["Array", 3, "pushback", 0, "pushback", 1, "pushback", 2, "getSize", "getCapacity"]
        // Expected Output: [null,null,null,null,3,3]
        System.out.println("\n--- Test Case 7 ---");
        dynamicArray = new DynamicArray(3);
        dynamicArray.pushback(0);
        System.out.println(dynamicArray);
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());

        // Test Case 8
        // Input: ["Array", 5, "pushback", 2, "pushback", 4, "set", 0, 0, "get", 0, "get", 1, "getSize", "getCapacity"]
        // Expected Output: [null,null,null,null,0,4,2,5]
        System.out.println("\n--- Test Case 8 ---");
        dynamicArray = new DynamicArray(5);
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        dynamicArray.pushback(4);
        System.out.println(dynamicArray);
        dynamicArray.set(0, 0);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.get(0));
        System.out.println(dynamicArray.get(1));
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());

        // Test Case 9
        // Input: ["Array", 1, "pushback", 1, "getCapacity", "pushback", 2, "getCapacity"]
        // Expected Output: [null,null,1,null,2]
        System.out.println("\n--- Test Case 9 ---");
        dynamicArray = new DynamicArray(1);
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getCapacity());
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getCapacity());

        // Test Case 10
        // Input: ["Array", 1, "getSize", "getCapacity", "pushback", 1, "getSize", "getCapacity", "pushback", 2,
        //         "getSize", "getCapacity", "get", 1, "set", 1, 3, "get", 1, "popback", "getSize", "getCapacity"]
        // Expected Output: [null,0,1,null,1,1,null,2,2,2,null,3,3,1,2]
        System.out.println("\n--- Test Case 10 ---");
        dynamicArray = new DynamicArray(1);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());
        System.out.println(dynamicArray.get(1));
        dynamicArray.set(1,3);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.get(1));
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());

        // Test Case 11
        // Input: ["Array", 1, "getSize", "getCapacity", "pushback", 1, "pushback", 2, "pushback", 3, "pushback", 4,
        //         "pushback", 5, "pushback", 6, "pushback", 7, "pushback", 8, "pushback", 9, "getSize", "getCapacity",
        //         "popback", "popback", "popback", "popback", "popback", "popback", "popback", "popback", "popback",
        //         "getSize", "getCapacity"]
        // Expected Output: [null,0,1,null,null,null,null,null,null,null,null,null,9,16,9,8,7,6,5,4,3,2,1,0,16]
        System.out.println("\n--- Test Case 11 ---");
        dynamicArray = new DynamicArray(1);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());
        dynamicArray.pushback(1);
        System.out.println(dynamicArray);
        dynamicArray.pushback(2);
        System.out.println(dynamicArray);
        dynamicArray.pushback(3);
        System.out.println(dynamicArray);
        dynamicArray.pushback(4);
        System.out.println(dynamicArray);
        dynamicArray.pushback(5);
        System.out.println(dynamicArray);
        dynamicArray.pushback(6);
        System.out.println(dynamicArray);
        dynamicArray.pushback(7);
        System.out.println(dynamicArray);
        dynamicArray.pushback(8);
        System.out.println(dynamicArray);
        dynamicArray.pushback(9);
        System.out.println(dynamicArray);
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.popback());
        System.out.println(dynamicArray.getSize());
        System.out.println(dynamicArray.getCapacity());
    }
}

