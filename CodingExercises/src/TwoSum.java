import java.util.HashMap;
import java.util.Map;

public class TwoSum {

    public static int[] twoSum(int[] nums, int target) {


        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (i == j) continue;

                if (nums[i] + nums[j] == target) return new int[]{i, j};

            }
        }

        return null;
    }

    public static int[] twoSumHashTable(int[] nums, int target) {

        Map<Integer, Integer> hashMap = new HashMap<>(nums.length);

        for (int i = 0; i < nums.length; i++) {
            hashMap.put(nums[i], i);
        }

        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (hashMap.containsKey(complement) && hashMap.get(complement) != i) {
                return new int[]{i, hashMap.get(complement)};
            }
        }

        return new int[]{};
    }


    public static void main(String[] args) {

        int[] nums = {2, 7, 11, 15};
        int target = 9;

        int[] result = twoSum(nums, target);

        if (result != null) {
            System.out.println("Indices: " + result[0] + ", " + result[1]);
            System.out.println("Values: " + nums[result[0]] + ", " + nums[result[1]]);
        } else {
            System.out.println("No two elements sum up to the target.");
        }

        target = 22;

        int[] resultHashTable = twoSumHashTable(nums, target);

        if (resultHashTable.length == 2) {
            int firstIndex = resultHashTable[0];
            int secondIndex = resultHashTable[1];
            System.out.println("Indices: " + firstIndex + ", " + secondIndex);
            System.out.println("Values: " + nums[firstIndex] + ", " + nums[secondIndex]);
        } else {
            System.out.println("No two elements sum up to the target.");
        }
    }
}
