import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ValidParentheses {

    public static boolean validParentheses(String string) {

        if (string.length() < 2
                || (string.charAt(0) != '(' && string.charAt(0) != '[' && string.charAt(0) != '{')
                || string.length() % 2 != 0)
            return false;

        char[] charHolder = new char[string.length()];
        int charHolderIndex = 0;


        for (int i = 0; i < string.length(); i++) {

            if (string.charAt(i) == '(' || string.charAt(i) == '[' || string.charAt(i) == '{') {
                charHolder[charHolderIndex] = string.charAt(i);
                charHolderIndex++;
            } else {

                if (charHolderIndex <= 0) return false;

                switch (string.charAt(i)) {
                    case ')':
                        if (charHolder[charHolderIndex - 1] != '(') {
                            return false;
                        }
                        charHolderIndex--;
                        break;
                    case ']':
                        if (charHolder[charHolderIndex - 1] != '[') {
                            return false;
                        }
                        charHolderIndex--;
                        break;
                    case '}':
                        if (charHolder[charHolderIndex - 1] != '{') {
                            return false;
                        }
                        charHolderIndex--;
                        break;


                }


            }

        }

        if (charHolderIndex > 0) return false;

        return true;
    }

    public static boolean validParenthesesWithStack(String string) {

        if (string.length() == 0
        || string.length() % 2 != 0
        || (string.charAt(0) != '(' && string.charAt(0) != '[' && string.charAt(0) != '{')
        ) {
            return false;
        }

        Stack<Character> characters = new Stack<>();

        for (char character : string.toCharArray()) {

            if (character == '(' || character == '[' || character == '{') {
                characters.push(character);
                continue;
            }

            if (character == ')' && characters.peek() == '('
                || character == ']' && characters.peek() == '['
                || character == '}' && characters.peek() == '{') {
                characters.pop();
            }

        }

        return characters.isEmpty();
    }

    public static void main(String[] args) {
        String test1 = "()";
        String test2 = "()[]{}";
        String test3 = "(]";
        String test4 = "([)]";
        String test5 = "{[]}";
        String test6 = "((()))";
        String test7 = "((()";
        String test8 = "))";
        String test9 = "";

        System.out.println("Test 1 Result: " + validParentheses(test1));
        System.out.println("Test 2 Result: " + validParentheses(test2));
        System.out.println("Test 3 Result: " + validParentheses(test3));
        System.out.println("Test 4 Result: " + validParentheses(test4));
        System.out.println("Test 5 Result: " + validParentheses(test5));
        System.out.println("Test 6 Result: " + validParentheses(test6));
        System.out.println("Test 7 Result: " + validParentheses(test7));
        System.out.println("Test 8 Result: " + validParentheses(test8));
        System.out.println("Test 9 Result: " + validParentheses(test9));

        System.out.println("Test 1 Result (Stack): " + validParenthesesWithStack(test1));
        System.out.println("Test 2 Result (Stack): " + validParenthesesWithStack(test2));
        System.out.println("Test 3 Result (Stack): " + validParenthesesWithStack(test3));
        System.out.println("Test 4 Result (Stack): " + validParenthesesWithStack(test4));
        System.out.println("Test 5 Result (Stack): " + validParenthesesWithStack(test5));
        System.out.println("Test 6 Result (Stack): " + validParenthesesWithStack(test6));
        System.out.println("Test 7 Result (Stack): " + validParenthesesWithStack(test7));
        System.out.println("Test 8 Result (Stack): " + validParenthesesWithStack(test8));
        System.out.println("Test 9 Result (Stack): " + validParenthesesWithStack(test9));
    }

}
